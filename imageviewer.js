'use strict';

//-----------------------------------------------------------------------------
// collection dependant variables

// base images URL
// NB: include trailing Slash
var picBase = '';

// small image file name suffix
var imgSmallSuffix = '_S.JPG';

// large image file name suffix
var imgLargeSuffix = '_L.JPG';

// small images directory (relative to the value of picBase)
// NB: include trailing Slash
var smallPath = 'small/';

// large images directory (relative to the value of picBase)
// NB: include trailing Slash
var largePath = 'large/';

// mask for formatting image sequence number
var seqMask = '_000';

// global variables
var itemId = '';
var curPic = 0;
var maxPic = 0;
var minPic = 0;
var curSize = 'S';
var argCount = 0;
var tmpPic = '';
var jumpFlag = false;


function writeCurrentPic() {
    // create img tag for current picture
    // with the full url to the current image

    var txtCP = '' + curPic;
    var seqText = seqMask.substr(0, seqMask.length - (txtCP.length)) + txtCP;

    var tmpPath = smallPath;
    var tmpSuffix = imgSmallSuffix;

    if (curSize == 'L') {
    tmpPath = largePath;
    tmpSuffix = imgLargeSuffix;
    }

tmpPic = picBase + itemId + '/' + tmpPath + itemId + seqText + tmpSuffix;

var theStr = '<img src="' + tmpPic + '" border="0" name="theimage"></img>'

document.write(theStr);
}

/**
 *
 * @returns {boolean}
 */
function goBack() {
    history.back();
    return false;
}

/**
 * Navigate to the next image.
 * @returns {boolean}
 */
function goNext() {
    curPic = curPic + 1;
    if (curPic > maxPic)curPic = minPic;
    changePic();
    return false;
}

/**
 * Navigate to the previous image.
 * @returns {boolean}
 */
function goPrev() {
    curPic = curPic - 1;
    if (curPic < minPic)curPic = maxPic;
    changePic();
    return false;
}


function jumpPic(selCtrl) {
    // Jump to specified image

    if (jumpFlag == true) {
        curPic = selCtrl.options[selCtrl.selectedIndex].value;
        changePic();
    }
}

/**
 * Change the current image.
 * @param newSize
 */
function changePic(newSize) {
    // dont jump before page initalises
    if (jumpFlag == false) {
        return;
    }
    // update current size if requested
    if (newSize && newSize != curSize) {
        curSize = newSize;
    }
    // load new image page
    var loc = document.location
    var href = loc.protocol + '//' + loc.hostname + loc.pathname + '?' + itemId + ',';
    if (argCount == 5)href = href + minPic + ',';
    href = href + maxPic + ',' + curPic + ',' + curSize;
    location.replace(href);
}

/**
 *
 * @returns {string}
 */
function popSelOpt() {
    var count = 0;
    var optStr = '';

    for (var loop = minPic; loop <= maxPic; loop = loop + 1) {
        count = count + 1;
        optStr = optStr + '<option value="' + loop + '"';
        if (loop == curPic)optStr = optStr + ' selected';
        optStr = optStr + '>' + count + '</option>';
    }
    return optStr;
}

/**
 * Set the image selection options.
 */
function setSelOpt() {
    var optCtrl = document.navForm.selopt.options[curPic - minPic];
    var sizeCtrl = document.navForm.selsize;
    optCtrl.selected = false;
    optCtrl.selected = true;
    for (var i = 0; i < sizeCtrl.length; i++) {
        if (sizeCtrl[i].value == curSize)sizeCtrl[i].checked = '1';
    }
    jumpFlag = true;
}


// parse the url query string for
// collection item identifier, image sequence range
// and current image of sequence to be displayed

var frag = document.location.search;
if (frag.length < 1)frag = '';

var args = frag.split(',');
var argsOK = false;


if (args.length > 1) {
    // itemid, max
    argCount = 3;

    curPic = 1;
    minPic = 1;
    curSize = 'S';

    itemId = args[0];
    itemId = itemId.substr(1);

    maxPic = parseInt(args[1]);

    // itemid, max, cur
    if (args.length > 2)curPic = parseInt(args[2]);

    // itemid, max, cur, size
    if (args.length > 3)curSize = args[3];

    // itemid, min, max, cur, size
    if (args.length > 4) {
        argCount = 5;

        minPic = parseInt(args[1]);
        maxPic = parseInt(args[2]);
        curPic = parseInt(args[3]);
        curSize = args[4];

    }
}


if (minPic > 0 &&
    maxPic > 0 &&
    curPic >= minPic &&
    curPic <= maxPic &&
    itemId.length > 0)argsOK = true;

if (!argsOK) {
    alert('Image Not Found');
    history.back();
}

var picCount = maxPic - minPic + 1;
